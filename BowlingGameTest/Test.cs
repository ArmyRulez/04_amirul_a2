﻿using NUnit.Framework;
using System;
using BowlingGame;

namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }

        void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }

        [Test]
        public void RollGutter()
        {
            RollMany(20, 0);

            Assert.That(game.Score, Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);

            Assert.That(game.Score, Is.EqualTo(20));
        }

        [Test]
        public void FirstFrameSpare()
        {
            game.Roll(9);
            game.Roll(1);
            RollMany(18, 1);

            Assert.That(game.Score, Is.EqualTo(29));
        }

        [Test]
        public void FirstFrameStrike()
        {
            game.Roll(10);
            RollMany(19, 1);

            Assert.That(game.Score, Is.EqualTo(30));
        }

        [Test]
        public void PerfectGame()
        {
            RollMany(21, 10);

            Assert.That(game.Score, Is.EqualTo(300));
        }

        [Test]
        public void AllSpareGame()
        {
            RollMany(21, 5);

            Assert.That(game.Score, Is.EqualTo(150));
        }

        [Test]
        public void TypicalGame()
        {
            game.Roll(10);
            game.Roll(9); game.Roll(1);
            game.Roll(5); game.Roll(5);
            game.Roll(7); game.Roll(2);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);
            game.Roll(9); game.Roll(0);
            game.Roll(8); game.Roll(2);
            game.Roll(9); game.Roll(1);
            game.Roll(10);

            Assert.That(game.Score, Is.EqualTo(187));
        }

        [Test]
        public void LastFrameStrike()
        {
            RollMany(18, 1);
            RollMany(3, 10);

            Assert.That(game.Score, Is.EqualTo(48));
        }
    }
}
