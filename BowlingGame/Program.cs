﻿using System;

namespace BowlingGame
{
    public class Game
    {
        int[] pinFalls = new int[21];
        int rollCounter;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public bool IsSpare(int i)
        {
            return pinFalls[i] + pinFalls[i + 1] == 10;
        }

        public bool IsStrike(int i)
        {
            return pinFalls[i] == 10;
        }

        public int SpareBonus(int i)
        {
            return pinFalls[i + 2];
        }

        public int StrikeBonus(int i)
        {
            return pinFalls[i + 1] + pinFalls[i + 2];
        }

        public void Roll(int pins)
        {
            pinFalls[rollCounter] += pins;
            rollCounter++;
        }

        public int Score()
        {
            int score = 0;
            int i = 0;

            for (int frame = 0; frame < 10; frame++)
            {
                if (IsStrike(i))
                {
                    score += 10 + StrikeBonus(i);
                    i += 1;
                }
                else if (IsSpare(i))
                {
                    score += 10 + SpareBonus(i);
                    i += 2;
                }
                else
                {
                    score += pinFalls[i] + pinFalls[i + 1];
                    i += 2;
                }
            }

            return score;
        }
    }
}
